#include <unistd.h>

#include <iostream>

#include <inttypes.h>
#include <queue>
#include <map>
#include <set>

#include <atomic>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <future>

using std::cin;
using std::cout;
using std::endl;

///Карта множетелей числа - ключ:число, значение:степень
using resultMap = std::map<u_int64_t, u_int64_t>;
static const resultMap sDefaultValue;

std::condition_variable cv;
std::mutex queueMutex;
std::queue<u_int64_t> sdataQueue;
std::atomic<bool> dataInputFinished{false};
static size_t sThreadLocalQueueSize;

//!
//! \brief Распарсить аргументы коммандной строки и проинициализировать значения
//! \param [in] argc - количество аргументов коммандной строки
//! \param [int] argv - аргументы коммандной строки
//! \param [out] threadCount - количество потоков
//! \param [out] useSet - не вычислять НОК для обработанных значений
//! \param [out] threadLockalBufferSize - размер локального буфера потока выполнения
//! \return
//!
bool parceStdArgs(const int& argc, char**argv, int&threadCount, bool& useSet, size_t& threadLockalBufferSize);

//!Показать спавку
void showHelp();

//!
//! \brief Объеденить две карты.
//! Объеденить две карты. Результат сохранить в первой карте.
//! В результирующую карту сохраняется наибольшее значение ключа
//! \param [in|out] a - первая карта
//! \param [in] b - вторая карта
//!
void merge(resultMap& a, const resultMap& b);


//!
//! \brief Разбор числа на множители
//! \param [in] value Натуральное число
//! \return Множетели числа
//!
resultMap factorize(u_int64_t value){
    u_int64_t delim = 2;
    resultMap delims;
    while (delim*delim <= value){
        if (value % delim != 0){
            delim+=1;
            continue;
        }
        delims[delim]++;
        value /=delim;
    }
    if (value > 1){
        delims[value]++;
    }
    return delims;
}


//!
//! \brief Обработка очереди
//! \return Карта НОК для всех обработанных чисел
//!
resultMap threadFunc(){
    resultMap res;
    while (1) {
        std::unique_lock<std::mutex> lk(queueMutex);
        cv.wait(lk, []()->bool {return !sdataQueue.empty() || dataInputFinished;});

        if (sdataQueue.empty() && dataInputFinished){
            break;
        }
        auto localQueueLength = std::min(sThreadLocalQueueSize, sdataQueue.size());

        std::vector<u_int64_t> localQueue;
        for (uint i = 0; i < localQueueLength; ++i){
            localQueue.emplace_back(sdataQueue.front());
            sdataQueue.pop();
        }
        lk.unlock();
        for (const auto& value: localQueue){
            auto valueNOD = std::move(factorize(value));
            merge(res, valueNOD);
        }
    }
    return res;
}



int main(int argc, char* argv[])
{
    int threadCount;
    //сохранять обработанные значения
    bool useSet;
    parceStdArgs(argc, argv, threadCount, useSet, sThreadLocalQueueSize);

    std::vector<std::future<resultMap>> futures;
    std::vector<std::thread> threads;
    dataInputFinished = false;

    for (int i = 0; i < threadCount; ++i){
        std::packaged_task<resultMap()> task([](){return threadFunc();});
        futures.push_back(task.get_future());
        threads.emplace_back(std::move(task));
    }

    //множество обработанных значений - если значение уже обработано, то смысла считать нок - нет
    std::set<u_int64_t> processedValues;
    auto isProcessed = [&processedValues](const u_int64_t&value)->bool {
        return processedValues.find(value) != processedValues.end();
    };

    //добавлять данные в очередь порционно
    std::vector<u_int64_t> localData;
    auto processLocalData = [&localData](){
        std::lock_guard<std::mutex> lk(queueMutex);
        for (auto& v: localData){
            sdataQueue.emplace(v);
        }
        localData.clear();
    };

    std::string line;
    while (std::getline(cin, line))
    {
        if (line.empty()){
            break;
        }
        u_int64_t value = atol(line.c_str());
        if (value < 2){
            continue;
        }
        if (useSet){
            if (isProcessed(value)){
                continue;
            }
            processedValues.insert(value);
        }

        if (localData.size() < sThreadLocalQueueSize){
            localData.emplace_back(value);
        }
        else{
            processLocalData();
            cv.notify_one();
        }

    }
    processLocalData();
    dataInputFinished = true;
    cv.notify_all();
    for (auto& t:threads){
        t.join();
    }

    resultMap res = std::move(futures.front().get());
    for (auto it = futures.begin()+1; it != futures.end(); ++it){
        merge(res, it->get());
    }
    if (res.size() > 0){
        std::cout<<"Least common multiple is:"<<std::endl;
        for (const auto &nod: res){
            std::cout<<nod.first<<" "<<nod.second<<"\n";
        }
    }
    return 0;
}


void merge(resultMap& a, const resultMap& b){
    for (const auto& kv: b){
        a[kv.first] = std::max(a[kv.first], kv.second);
    }
}

bool parceStdArgs(const int& argc, char**argv, int&threadCount, bool& useSet, size_t& threadLockalBufferSize){
    threadCount = 4;
    useSet = false;
    threadLockalBufferSize = 5;

    if (argc > 1){
        //шаблон строки параметров
        static const char* optstr = "j:l:sh?";
        auto param = getopt(argc, argv, optstr);
        while (param!=-1){
            switch (param) {
            case 'j':{
                threadCount = atoi(optarg);
                if (threadCount < 1){
                    threadCount = 1;
                }
                break;
            }
            case 's':
                useSet = true;
                break;
            case 'l':
                threadLockalBufferSize = atol(optarg);
                if (threadLockalBufferSize < 1){
                    threadLockalBufferSize = 5;
                }
                break;
            case 'h':
            case '?':{
               showHelp();
               return false;
            }
            default:
                break;
            }
            param = getopt(argc, argv, optstr);
        }
    }
    return true;
}

void showHelp(){
    std::cout<<"\tLeast common multiple counter programm"<<std::endl
             <<"Numbers are read from stdin. To finish press Ctrl+D or enter empty line."<<std::endl
             <<"Usage:"<<std::endl
             <<"-j threads: set number of threads. Default value is 4"<<std::endl
             <<"-s : use Set. Dont count LCM for already processed values"<<std::endl
             <<"-l value: thread lockal queue size. Default is 5"<<std::endl
             <<"-h | ? : show this message."<<std::endl<<std::endl;
}
