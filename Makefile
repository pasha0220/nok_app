TARGET=nok_app
SOURCES=main.cpp
OBJECTS=$(SOURCES:.cpp=.o)

CC=g++
CFLAGS=-c -Wall -pipe -pthread -std=c++11 -g -Wall -W -fPIE
LDFLAGS=
LIBS=-pthread

all: $(SOURCES) $(TARGET)
	
$(TARGET): $(OBJECTS) 
	$(CC) $(LDFLAGS) -o $(TARGET) $(OBJECTS) $(LIBS)

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -rf *.o nok_app
